var webpackConfig = require('./webpack.config.js')

module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt)

  grunt.initConfig({
    webpack: {
      options: webpackConfig({ production: true })
    , dist: { cache: false }
    }
  , 'webpack-dev-server': {
      options: {
        webpack: webpackConfig()
      , publicPath: '/'
      , port: 8000
      , hot: true
      }
    , start: {
        keepalive: true
      , watch: true
      }
    }
  })

  grunt.registerTask('s'
    , '`grunt s` is alias task for `grunt webpack-dev-server`'
    , ['webpack-dev-server'])

}
